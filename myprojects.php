<?php
session_start();
include ("functions.php");
$link = getLink();
checkSession();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">


    <title>MyProjects</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>
<script>
    //https://datatables.net/
    $(document).ready( function () {
        $('.dtable').DataTable();
    } );
</script>
<body style="background-color:#4B5DFB;">
    <a href="profile.php">
        <h1 style="margin-left: 30px;margin-top: 20px; background-color:#4b5dfb
; color:ivory;">Home</h1>
    </a>
    <h1 style="color: white;font-family: arial, sans-serif;font-size: 36px;font-weight: bold;margin-top: 0px;margin-bottom: 1px;text-align: center;">My Projects</h1>
<?php
$query="Select * from projects 
inner join users on users.idUser=projects.idUser 
WHERE users.email='" . $_SESSION["email"] . "' and users.password = '" . $_SESSION["password"] . "'";

$result=mysqli_query($link,$query);
if (mysqli_num_rows($result)>0){
    while($row=mysqli_fetch_assoc($result)){

        $msg="";

        $query2 = "Select firstname,lastname,investmentFund,investmentDate from projects_investors 
        inner join users on users.idUser=projects_investors.idUser 
        WHERE projects_investors.idProject='" . $row["idProject"] . "'";
        $result2 = mysqli_query($link, $query2);
        ?>

        <h2 style="background-color:#4B5DFB; color:aliceblue;"><?php echo $row["projectName"]?></h2>
                    <p style="background-color:#4B5DFB; color:aliceblue;"><?php echo $row['projectDescription']?></p>
                    <p style="font-size: 120%; ">Start Date: <?php echo $row['projectStartDate']?></p>
            <p style="font-size: 120%;">End Date: <?php echo $row['projectEndDate']?></p>
            <p style="font-size: 150%;color: #0a2ba6">Requested Fund: <?php echo $row['requestedFund']?></p>
        <h2 style="background-color:#4B5DFB; color:aliceblue;">Investors: </h2>
            <div style="margin:0 40px 40px 40px">
                <table class="dtable">
                    <thead>
                    <tr>
                        <td style="border: 1px solid black; background-color:white;">Firstname</td>
                                                <td style="border: 1px solid black; background-color:white;"> Lastname</td>
                                                <td style="border: 1px solid black; background-color:white;">Investment Fund</td>
                                                <td style="border: 1px solid black; background-color:white;">Investment Date</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while($row2 = mysqli_fetch_assoc($result2)) {
                        ?>
                            <tr>

                                <td style="border: 1px solid black;" ><?php echo $row2['firstname']?></td>
                                                                <td style="border: 1px solid black;"><?php echo $row2['lastname']?></td>
                                                                <td style="border: 1px solid black;"><?php echo $row2['investmentFund']?></td>
                                                               <td style="border: 1px solid black;"><?php echo $row2['investmentDate']?></td>
                            </tr>

                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
    <?php
    }
}

else{
    ?>
    <h1 style="
    color: white;
font-family: arial, sans-serif;
font-size: 36px;
font-weight: bold;
margin-top: 0px;
margin-bottom: 1px;
text-align:inherit;
padding: 45px;">Unfortunately you don't have any projects</h1>

    <?php
    }
?>
</body>
</html>
