<?php
session_start();
include ("functions.php");
$mes="";
if (isset($_GET["message"])){
    $mes=$_GET["message"];
}

$link = getLink();
if (!isset($_SESSION['projectName'])){
    $_SESSION['projectName']= $_GET['projectName'];
}

checkSession();
$query="Select * from projects where projectName='" . $_SESSION["projectName"] . "'";
$result=mysqli_query($link,$query);
$row=mysqli_fetch_assoc($result);
$_SESSION["idProject"]=$row["idProject"];

?>

<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Login Form</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <title>Gallery</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">


</head>
<body style="background-color: #3c73ff;">
<a href="profile.php">
<h1 style="color:aliceblue;
font-family: arial, sans-serif;
font-size: 36px;
font-weight:under;
text-decoration: underline;
margin-top: 0px;
margin-bottom: 1px;
padding: 30px;">Home</h1>
</a>
<section class="container">
    <div class="login">

        <h1 style="color: rebeccapurple;
font-family: arial, sans-serif;
font-size: 24px;
font-weight: bold;
margin-top: 0px;
margin-bottom: 1px;
text-align: center;">Investing</h1>
        <h2 style="color: #111; font-family: 'Open Sans', sans-serif; font-size: 30px; font-weight: 300; line-height: 32px; margin: 0 0 72px; text-align: center;"><?php echo $_SESSION["projectName"]?></h2>
        <h3 style="color: darkred"><?php echo $mes?></h3>
        <p style="color:aliceblue"><?php echo $row['projectDescription']?></p>
        <p style="color:#111;: 100%;">Start Date: <?php echo $row['projectStartDate']?></p>
        <p style="color:#111;font-size: 100%;">End Date: <?php echo $row['projectEndDate']?></p>
        <p style="font-size: 150%;color: #0a2ba6">Requested Fund: <?php echo $row['requestedFund']?></p>

        <form method="post" action="makeinvestment.php">

            <input type="text" name="amount" id="amount" placeholder="Amount" required>
            <p class="submit"><input type="submit" name="commit" value="Invest" style="background-color:cornflowerblue"></p>
        </form>
    </div>
</section>
</body>
</html>




