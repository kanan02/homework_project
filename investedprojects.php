<?php
session_start();
include ("functions.php");
$link = getLink();
checkSession();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">


    <title>Invested Projects</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
</head>
<script>
    //https://datatables.net/
    $(document).ready( function () {
        $('.dtable').DataTable();
    } );
</script>
<body style="background-color:#4b5dfb">
<a href="profile.php">
    <h1 style="background-color:#4b5dfb;
+    color:aliceblue;
+font-family: arial, sans-serif;
+font-size: 36px;
+font-weight: bold;
+margin-top: 0px;
+margin-bottom: 1px;">Home</h1>
</a>
<h1 style="background-color:#4B5DFB;
+color:aliceblue;
+font-family: arial, sans-serif;
+font-size: 36px;
+font-weight: bold;
+margin-top: 0px;
+margin-bottom: 1px;">Invested Projects</h1>
<?php
$query="Select * from projects_investors
inner join users on users.idUser=projects_investors.idUser 
inner join projects on projects.idProject=projects_investors.idProject 

WHERE users.email='" . $_SESSION["email"] . "' and users.password = '" . $_SESSION["password"] . "'";

$result=mysqli_query($link,$query);
mysqli_num_rows($result);
if (mysqli_num_rows($result)>0) {
    ?>
    <div>
                <table class="dtable" style="border: 1px solid black;" >
                        <thead style="border: 1px solid black;">
            <tr>
                <td style="border: 1px solid black; background-color:white; " >Project Name</td>
                                <td style="border: 1px solid black; background-color:white; ">Project Description</td>
                                <td style="border: 1px solid black; background-color:white;" >Project StartDate</td>
                                <td style="border: 1px solid black; background-color:white;">Project EndDate</td>
                                <td style="border: 1px solid black; background-color:white;">Requested Fund</td>
                                <td style="border: 1px solid black; background-color:white;">Investment Fund</td>
                                <td style="border: 1px solid black; background-color:white;">Investment Date</td>
            </tr>
            </thead>
            <tbody>
            <?php
            while($row = mysqli_fetch_assoc($result)) {
                ?>
                <tr>
                    <td style="border: 1px solid black  ;" ><?php echo  $row["projectName"]?></td>
                    <td style="border: 1px solid black;"><?php echo $row['projectDescription']?></td>
                    <td style="border: 1px solid black;"><?php echo $row['projectStartDate']?></td>
                    <td style="border: 1px solid black;"><?php echo $row['projectEndDate']?></td>
                    <td style="border: 1px solid black;"><?php echo $row['requestedFund']?></td>
                    <td style="border: 1px solid black;"><?php echo $row['investmentFund']?></td>
                    <td style="border: 1px solid black;"><?php echo $row['investmentDate']?></td>
                </tr>

                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <?php
}
else{
    ?>
    <h2
                style="color: black;
+font-family: arial, sans-serif;
+font-size: 24px;
+font-weight: bold;
+margin-top: 0px;
+margin-bottom: 1px;
+text-align:inherit;
+padding: 25px;">Unfortunately you haven't invested in any project yet</h2>
    <p>You can do it in your home page</p>

    <?php
}
?>
</body>
</html>
