<?php
session_start();
include ("functions.php");
$link = getLink();
checkSession();
$query="Select * from projects";
$query2="Select * from projects
inner join projects_investors on projects.idProject=projects_investors.idProject
inner join users on users.idUser=projects_investors.idUser
WHERE users.email='" . $_SESSION["email"] . "' and users.password = '" . $_SESSION["password"] . "'";
$query3="Select * from projects
inner join users on users.idUser=projects.idUser
WHERE users.email='" . $_SESSION["email"] . "' and users.password = '" . $_SESSION["password"] . "'";
$result=mysqli_query($link,$query);
$result2=mysqli_query($link,$query2);
$result3=mysqli_query($link,$query3);
$arr1 = [];
$arr2 = [];
while ($row2=mysqli_fetch_assoc($result2)){
    array_push($arr1,$row2['projectName']);
}
while ($row3=mysqli_fetch_assoc($result3)){
    array_push($arr2,$row3['projectName']);
}
?>


<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">


    <title>Crowdfunding</title>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" type="text/css">
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" href="css/bootstrap.min.css">

    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="css/responsive.css">

    <link rel="icon" href="images/favicon.png" type="image/gif" />

    <link rel="stylesheet" href="css/jquery.mCustomScrollbar.min.css">

    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

</head>
<script>
    //https://datatables.net/
    $(document).ready( function () {
        $('.dtable').DataTable();
    } );
</script>
<body>
<div id="mySidebar" class="sidebar">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    <a href="index.php">Home</a>
    <a href="logout.php">Logout</a>
</div>
<div id="main">

    <div class="header_section">
        <div class="container-fluid">
            <div style="margin-bottom: 30px" class="row">
            </div>
            <div class="row" style="font-size: 120%;">
                <div class="col-sm-1">
                    <div class="search_icon"><img height="50" width="50" src="images/search-icon.png"></div>
                </div>
                <div class="col-sm-3">
                    <h1 style="color: white;">
                        <?php
                        echo $_SESSION["firstname"];
                        echo " ";
                        echo $_SESSION["lastname"];
                        ?>

                    </h1>
                </div>
                <div class="col-sm-3" >
                    <a href="investedprojects.php" style="color: white">Invested projects</a>
                </div>
                <div class="col-sm-3">
                    <a href="myprojects.php" style="color: white">My projects</a>


                </div>

                <div class="col-sm-2">
                    <div class="togle_main"><a class="class="openbtn onclick="openNav()"><img src="images/togle-menu-icon.png" style="max-width: 100%;"></a></div>

                </div>
            </div>
        </div>
    </div>
</div>
<br><br><br><br><br>
<main>
    <div style="margin:0 40px 40px 40px">
        <table class="dtable">
            <thead>
            <tr>
                <td style="border: 1px solid black;">Project Name</td>
                <td style="border: 1px solid black;"> Project Description</td>
                <td style="border: 1px solid black;">Requested Fund</td>
                <td style="border: 1px solid black;">Project End Date</td>
                <td style="border: 1px solid black;"> Invest</td>
            </tr>
            </thead>
            <tbody>
            <?php
            while($row = mysqli_fetch_assoc($result)) {
                $isok=true;

                foreach ($arr1 as $element) {
                    if ($element==$row['projectName']){
                        $isok=false;
                        break;
                    }
                }
                foreach ($arr2 as $element) {
                    if ($element==$row['projectName']){
                        $isok=false;
                        break;
                    }
                }
                if($isok){
                    ?>
                    <tr>

                        <td style="border: 1px solid black;"><?php echo $row['projectName']?></td>
                        <td style="border: 1px solid black;"><?php echo $row['projectDescription']?></td>
                        <td style="border: 1px solid black;"><?php echo $row['requestedFund']?></td>
                        <td style="border: 1px solid black;"><?php echo $row['projectEndDate']?></td>
                        <td style="border: 1px solid black;">
                            <?php echo "<a href=\"invest.php?projectName=".$row['projectName']."\" style='font-size: 120%;color: green;border: 2.5px solid green;border-radius: 10%;padding: 3px;' >Invest</a>"?>
                        </td>

                    </tr>
                    <?php
                }
                ?>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</main>

<div class="footer_section layout_padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h2 class="important_text">Important Link</h2>
                <div class="footer_menu">
                    <ul>
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Our Latest Event</a></li>
                        <li><a href="#">Our Latest Article</a></li>
                        <li><a href="#">Join With Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <h2 class="important_text">Social Link</h2>
                <div class="footer_menu">
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Linkedin</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Youtube</a></li>
                        <li><a href="#">Pinterest</a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>



<script>
    $(document).ready(function() {
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
        $('#myCarousel').carousel({
            interval: false
        });
    });
</script>

<script>
    function openNav() {
        document.getElementById("mySidebar").style.width = "250px";
        document.getElementById("main").style.marginLeft = "250px";
    }

    function closeNav() {
        document.getElementById("mySidebar").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
    }
</script>
</body>
</html>


