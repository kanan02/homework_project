<?php
session_start();
unset($_SESSION["name"]);
unset($_SESSION["idUser"]);
unset($_SESSION["email"]);
unset($_SESSION["password"]);
unset($_SESSION["firstname"]);
unset($_SESSION["lastname"]);
session_unset();
session_destroy();
header("Location:login.html");


